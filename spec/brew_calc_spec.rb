describe "Brew Calc Class" do
  before do
    @sgrav = GravityReading.new
    @sgrav.gravity = 1.050
    @fgrav = GravityReading.new
    @fgrav.gravity = 1.000
    @calibration_temp = 68
  end

  it "sets up correctly" do
    @sgrav.gravity.should == 1.050
  end

  it "calculates ABV from two gravity readings" do
    abv = BrewCalc.abv(@sgrav.calibrated_gravity, @fgrav.calibrated_gravity)
    abv.round(4).should.be == (0.066666).round(4)
  end

  it "calculates ABW from two gravity readings" do
    abw = BrewCalc.abw(@sgrav.calibrated_gravity, @fgrav.calibrated_gravity)
    abw.round(4).should.be == (0.052666).round(4)
  end

  it "can set the calibration temperature of a reading" do
    @sgrav.temperature = 72
    abv = BrewCalc.abv(@sgrav.calibrated_gravity, @fgrav.calibrated_gravity)
    abv.round(4).should.be == (0.067333).round(4)
  end

  it "can set the calibration temperature of the hydrometer" do
    #BrewCalc.
  end
end
