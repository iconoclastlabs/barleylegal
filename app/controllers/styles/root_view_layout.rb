# Root View's Layout and Styles
class RootViewLayout < MK::Layout
  TOP_MARGIN  = 0
  LEFT_MARGIN = 15

  view :settings_button
  view :start_gravity_slider
  view :start_gravity_value
  view :start_temp_slider
  view :start_temp_value

  view :final_gravity_value
  view :final_gravity_slider
  view :final_temp_value
  view :final_temp_slider

  view :abw_value
  view :abv_value

  def layout # implicitly creates a UIView on your view tree
    background_color :white.uicolor

    add UIView, :background do
      @background_image = add(UIImageView, :bg_image)

      # @items = ['ºF','ºC']
      # @temp_settings = add(UISegmentedControl.alloc.initWithItems(@items), :temp_settings)

      add(UILabel, :start_header)
      add(UILabel, :start_gravity_label)
      @start_gravity_value  = add(UILabel, :start_gravity_value)
      @start_gravity_slider = add(UISlider, :start_gravity_slider)
      add(UILabel, :start_temp_label)
      @start_temp_value  = add(UILabel, :start_temp_value)
      @start_temp_slider = add(UISlider, :start_temp_slider)

      add(UILabel, :final_header)
      add(UILabel, :final_gravity_label)
      @final_gravity_value  = add(UILabel, :final_gravity_value)
      @final_gravity_slider = add(UISlider, :final_gravity_slider)
      add(UILabel, :final_temp_label)
      @final_temp_value  = add(UILabel, :final_temp_value)
      @final_temp_slider = add(UISlider, :final_temp_slider)

      @results_view = add UIView, :results_view do
        add(UILabel, :results_label)
        add(UILabel, :abw_label)
        @abw_value = add(UILabel, :abw_value)
        add(UILabel, :abv_label)
        @abv_value = add(UILabel, :abv_value)
      end
    end
  end

  def add_constraints(controller)
    unless @layout_constraints_added
      @layout_constraints_added = true
      constraints do
        ap superview
      end
    end
  end

  def background_style
    backgroundColor :clear.uicolor
    portrait do
      x 0
      y TOP_MARGIN
      width '100%'
      height '100%'
    end
  end

  def label_style
    number_of_lines 1
    text_color :black.uicolor
    background_color :clear.uicolor
  end

  def bg_image_style
    frame [[0, 20], ['100%', '100%']]
    image 'images/background'.uiimage
  end

  def settings_button_style
    frame [[275.0, 34.0], [30.0, 30.0]]
    background_color :clear.uicolor
  end

  def gear_image_style
    # frame [[0, 0], [@settings_button.width, @settings_button.height]]
    frame [[0, 0], [30, 30]]
    image 'images/Gear'.uiimage
  end

  def start_header_style
    text 'Starting Readings'
    font 'HelveticaNeue-Thin'.uifont(30)
    frame [[LEFT_MARGIN, 30], [230, 40]]
    text_alignment :left.nsalignment
  end

  def start_gravity_label_style
    text 'Specific Gravity'
    frame [[LEFT_MARGIN, 77.0], [150.0, 25.0]]
    font 'HelveticaNeue-Thin'.uifont(18)
    text_alignment :left.nsalignment
  end

  def start_gravity_value_style
    text '1.050'
    frame [[230.0, 77.0], [75.0, 25.0]]
    font 'HelveticaNeue-Thin'.uifont(18)
    text_alignment :right.nsalignment
  end

  def start_gravity_slider_style
    frame [[LEFT_MARGIN, 100.0], [290.0, 34.0]]
    minimum_value 1.000
    maximum_value 1.150
    value 1.05
  end

  def start_temp_label_style
    text 'Temperature'
    frame [[LEFT_MARGIN, 137.0], [100.0, 25.0]]
    font 'HelveticaNeue-Thin'.uifont(18)
    text_alignment :left.nsalignment
  end

  def start_temp_slider_style
    frame [[LEFT_MARGIN, 160.0], [290.0, 34.0]]
    case App::Persistence['temp_unit']
    when 'farenheit'
      minimum_value 50
      maximum_value 120
      value App::Persistence['calibration_temp']
    when 'celcius'
      minimum_value 10
      maximum_value 50
      value (App::Persistence['calibration_temp']-32)/1.8
    end
  end

  def start_temp_value_style
    text '60.0º F'
    frame [[228.0, 137.0], [75.0, 25.0]]
    font 'HelveticaNeue-Thin'.uifont(18)
    text_alignment :right.nsalignment
  end


  def final_header_style
    frame [[LEFT_MARGIN, 210], [200, 40]]
    text 'Final Readings'
    font 'HelveticaNeue-Thin'.uifont(30)
    text_alignment :left.nsalignment
  end

  def final_gravity_label_style
    frame [[LEFT_MARGIN, 257.0], [255.0, 25.0]]
    text 'Specific Gravity'
    font 'HelveticaNeue-Thin'.uifont(18)
    text_alignment :left.nsalignment
  end

  def final_gravity_value_style
    frame [[230.0, 257.0], [75.0, 25.0]]
    text '1.010'
    font 'HelveticaNeue-Thin'.uifont(18)
    text_alignment :right.nsalignment
  end

  def final_gravity_slider_style
    frame [[LEFT_MARGIN, 280.0], [290.0, 34.0]]
    minimum_value 1.000
    maximum_value 1.150
    value 1.01
  end

  def final_temp_label_style
    frame [[LEFT_MARGIN, 317.0], [100.0, 25.0]]
    text 'Temperature'
    font 'HelveticaNeue-Thin'.uifont(18)
    text_alignment :left.nsalignment
  end

  def final_temp_value_style
    frame [[228.0, 317.0], [75.0, 25.0]]
    text '60.0º F'
    font 'HelveticaNeue-Thin'.uifont(18)
    text_alignment :right.nsalignment
  end

  def final_temp_slider_style
    frame [[LEFT_MARGIN, 340.0], [290.0, 34.0]]
    case App::Persistence['temp_unit']
    when 'farenheit'
      minimum_value 50
      maximum_value 120
      value App::Persistence['calibration_temp']
    when 'celcius'
      minimum_value 10
      maximum_value 50
      value (App::Persistence['calibration_temp'] - 32) / 1.8
    end
  end

  def results_view_style
    frame [[0, '70%+20'], ['100%', '30%']]
    background_color :white.uicolor(0.5)
  end

  def results_label_style
    frame [[LEFT_MARGIN,0], [100,50]]
    text 'Results'
    font 'HelveticaNeue-Thin'.uifont(30)
    text_alignment :left.nsalignment
  end

  def abw_label_style
    frame [[LEFT_MARGIN, 55], [150.0, 25.0]]
    text 'Alcohol By Weight:'
    font 'HelveticaNeue-Thin'.uifont(18)
    text_alignment :left.nsalignment
  end

  def abw_value_style
    frame [[240.0, 55.0], [60.0, 25.0]]
    text '4.13%'
    font 'HelveticaNeue-Thin'.uifont(18)
    text_alignment :right.nsalignment
  end

  def abv_label_style
    frame [[LEFT_MARGIN, 90], [150.0, 25.0]]
    text 'Alcohol By Volume:'
    font 'HelveticaNeue-Thin'.uifont(18)
    text_alignment :left.nsalignment
  end

  def abv_value_style
    frame [[240, 90], [60.0, 25.0]]
    text '5.36%'
    font 'HelveticaNeue-Thin'.uifont(18)
    text_alignment :right.nsalignment
  end
end
