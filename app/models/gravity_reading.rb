class GravityReading
  attr_accessor :name, :gravity, :temperature, :calibration_temperature
  def initialize()
    # Specific Gravity as measured by the hydrometer.
    @gravity ||= 1.050
    # Temperature the gravity reading was taken at.
    @temperature ||= 68
    # Calibrated temperature at which the user's hydrometer reads 1.000,
    # most are calibrated to 68 and this won't need to change, though some are 71.
    @calibration_temperature ||= 68
  end

  # Method that calculates the actual gravity of a hydrometer reading if the 
  # reading was taken above or below the calibration temperature.
  def calibrated_gravity
    BrewCalc.hydrometer_correction(@gravity, @temperature, @calibration_temperature)
  end
end
