class AppDelegate
  def application(application, didFinishLaunchingWithOptions:launchOptions)
    @window = UIWindow.alloc.initWithFrame(UIScreen.mainScreen.bounds)
    root_controller = RootViewController.alloc.init
    @window.rootViewController = root_controller
    @window.makeKeyAndVisible
  end
end
