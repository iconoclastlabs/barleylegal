class UISegmentedControlLayout < MK::UIViewLayout
  targets UISegmentedControl

  # platform specific default root view
  def default_root
    p "Custom Layout called"
    # child Layout classes will return *their* UIView subclass from self.targets
    view_class = self.class.targets || MotionKit.default_view_class
    view_class.alloc.initWithItems(['yes','no'])
  end

end
