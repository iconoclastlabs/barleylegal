class BrewCalc

  # These should be user-configurable values since grain varieties can absorb different
  # amounts of water, though these are good rule-of-thumb starting values.
  @@grain_absorbtion_ratio = 0.13  # gallons/pound of water absorbed by the grain
  @@water_to_grain_ratio =  0.31    # typical amount of gallons per pound of grain
  @@calibration_temp = 68        # Fahrenheit
  # Conversion Equations
  # sg =  specific gravity
  # plato = alternate SG unit
  def self.sg_to_plato (grav)
    (668.72 * grav) - 463.37 - (205.347 * (grav * grav))
  end
  # and reverse
  def self.plato_to_sg (p)
    (((0.082636 + (3.8480 * p) + (0.014563 * (p*p) ))) / 1000) + 1
  end
  # 1 deg Brix = 1.04 degree Plato
  def self.plato_to_brix(p)
    return p/(1.04)
  end
  def self.brix_to_plato(b)
    return (b * 1.04)
  end
  # GU = Gravity units, another alternative
  def self.sg_to_gu(sg)
    (sg-1) * 1000
  end

  # Calculates Alcobol by Volume
  # og = original gravity measured (starting specific gravity measurement)
  # fg = final gravity measured (final specific gravity measurement)
  def self.abv(og, fg)
    (og - fg) / 0.75
  end
  # Calculates Alcohol by Weight
  def self.abw(og, fg)
    v = abv(og, fg)
    (0.79*v) / fg
  end

  # Hydrometer Correction - temps in F
  # Hydrometer readings are affected by the liquid's temperature. Most are "calibrated" to 60F,
  # which means if your liquid temp is above/below 60F, your reading will be wrong and it will
  # need to be corrected for by a temperature measurement.
  # Also, calibration_temp is accepted as a parameter since not all hydrometers are set for 60F,
  # some read water's gravity at 1.00 at 68F instead.
  # use example: BeerCalc.hydrometer_correction(1.55, 72)
  def self.hydrometer_correction(measured_gravity, measured_temp, calibration_temp)
    mt2 = measured_temp * measured_temp
    mt3 = mt2 * measured_temp
    ct2 = calibration_temp * calibration_temp
    ct3 = ct2 * calibration_temp
    numerator   = (1.00130346 - 0.000134722124 * measured_temp    + 0.00000204052596 * mt2 - 0.00000000232820948 * mt3)
    denominator = (1.00130346 - 0.000134722124 * calibration_temp + 0.00000204052596 * ct2 - 0.00000000232820948 * ct3)
    (measured_gravity * (numerator / denominator))
  end

  # Batch Sparge Calculator
  # calulates how much water is needled to sparge your wort
  def self.batch_sparge(grain_weight, boil_vol)
    strike_water_volume = @@water_to_grain_ratio * grain_weight
    grain_absorb_vol = @@grain_absorbtion_ratio * grain_weight
    first_sparge = (boil_vol / 2) - (strike_water_volume - grain_absorb_vol)
    second_sparge = (boil_vol / 2)
  {strike_water_volume: strike_water_volume, grain_absorb_vol: grain_absorb_vol, first_sparge: first_sparge, second_sparge: second_sparge}
  end

  # Strike Temperature Calculator
  # calculates how hot your strike water needs to be to reach your mash's target temperature
  # Dry grain temp is approximately the ambient air temperature.
  def self.strike_temp(mash_target_temp, dry_grain_temp)
    (0.2 * (@@water_to_grain_ratio * 4)) * (mash_target_temp - dry_grain_temp) + mash_target_temp
  end
end
