class RootViewController < UIViewController

  def loadView
    App::Persistence['temp_unit'] ||= 'farenheit'
    App::Persistence['calibration_temp'] ||= 60

    @layout = RootViewLayout.new
    self.view = @layout.view

    $layout = @layout

    @start_gravity_slider = @layout.start_gravity_slider
    @start_gravity_value  = @layout.start_gravity_value
    @start_temp_slider    = @layout.start_temp_slider
    @start_temp_value     = @layout.start_temp_value

    @final_gravity_slider = @layout.final_gravity_slider
    @final_gravity_value  = @layout.final_gravity_value
    @final_temp_slider    = @layout.final_temp_slider
    @final_temp_value     = @layout.final_temp_value

    @abw_value            = @layout.abw_value
    @abv_value            = @layout.abv_value

    case App::Persistence['temp_unit']
    when 'farenheit'
      @start_temp_value.text = '60.0º F'
      @final_temp_value.text = '60.0º F'
    when 'celcius'
      @start_temp_value.text = '15.5º C'
      @final_temp_value.text = '15.5º C'
    end

    @start_gravity_slider.addTarget(self,
      action:'start_gravity_slider_changed',
      forControlEvents:UIControlEventValueChanged)
    @start_temp_slider.addTarget(self,
      action:'start_temp_slider_changed',
      forControlEvents:UIControlEventValueChanged)
    @final_gravity_slider.addTarget(self,
      action:'final_gravity_slider_changed',
      forControlEvents:UIControlEventValueChanged)
    @final_temp_slider.addTarget(self,
      action:'final_temp_slider_changed',
      forControlEvents:UIControlEventValueChanged)

  end

  def updateViewConstraints
    @layout.add_constraints(self)
    super
  end

  def start_gravity_slider_changed
    @start_gravity_value.text = "#{@start_gravity_slider.value.round(3)}"
    update_results
  end

  def start_temp_slider_changed
    case App::Persistence['temp_unit']
    when 'farenheit'
      @start_temp_value.text = "#{@start_temp_slider.value.round(1)}º F"
    when 'celcius'
      @start_temp_value.text = "#{@start_temp_slider.value.round(1)}º C"
    end
    update_results
  end

  def final_gravity_slider_changed
    @final_gravity_value.text = "#{@final_gravity_slider.value.round(3)}"
    update_results
  end

  def final_temp_slider_changed
    case App::Persistence['temp_unit']
    when 'farenheit'
      @final_temp_value.text = "#{@final_temp_slider.value.round(1)} ºF"
    when 'celcius'
      @final_temp_value.text = "#{@final_temp_slider.value.round(1)} ºC"
    end
    update_results
  end

  def update_results
    starting_gravity = GravityReading.new
    starting_gravity.name = 'Starting Gravity'
    starting_gravity.temperature = @start_temp_slider.value
    starting_gravity.calibration_temperature = App::Persistence['calibration_temp']
    starting_gravity.gravity = @start_gravity_slider.value

    final_gravity = GravityReading.new
    final_gravity.name = 'Final Gravity'
    final_gravity.temperature = @final_temp_slider.value
    final_gravity.calibration_temperature = App::Persistence['calibration_temp']
    final_gravity.gravity = @final_gravity_slider.value

    @abw_value.text = "#{(BrewCalc.abw(starting_gravity.calibrated_gravity, final_gravity.calibrated_gravity) * 100).round(2)}%"
    @abv_value.text = "#{(BrewCalc.abv(starting_gravity.calibrated_gravity, final_gravity.calibrated_gravity) * 100).round(2)}%"
  end
end
